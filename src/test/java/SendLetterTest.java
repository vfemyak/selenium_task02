import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SendLetterTest {

    WebDriver driver = new ChromeDriver();
    String login = "xxxxxxx";
    String password = "xxxxxxx";

    @BeforeClass
    public static void init(){
        System.setProperty("webdriver.chrome.driver","C:/Users/vfemy/IdeaProjects/chromedriver.exe");
    }

    @Test
    public void testSending() throws InterruptedException {
        driver.get("https://www.google.com/gmail/");

        driver.findElement(By.cssSelector("input[type=\'email\']")).sendKeys(login);
        driver.findElement(By.xpath("//div[@class=\'dG5hZc\']//span")).click();

        Assert.assertEquals("gmail",driver.getTitle().toLowerCase());

       // (new WebDriverWait(driver,10)).until((dr)->dr.getTitle().toLowerCase().startsWith("gmail"));
        Thread.sleep(2000);

        driver.findElement(By.cssSelector("input[type=\'password\']")).sendKeys(password);
        driver.findElement(By.xpath("//div[@class=\'dG5hZc\']//span")).click();

        Thread.sleep(6000);

        Assert.assertTrue(driver.getTitle().contains(login));

        driver.findElement(By.xpath("//div[@class=\'z0\']/descendant::div[@role=\'button\']")).click();
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("div.wO textarea.vO")).sendKeys("jofol@webmails.top");
        driver.findElement(By.cssSelector("input.aoT[name=\'subjectbox\']")).sendKeys("TestLetter");
        driver.findElement(By.xpath("//table[@class=\'iN\']/descendant::*[@role=\'textbox\']"))
                .sendKeys("Sample");
        driver.findElement(By.xpath("//table[@class=\'IZ\']/descendant::*[@role=\'button\']")).click();
        Thread.sleep(3000);

        driver.quit();
    }

}
